/*
* AUTHOR: Margaux
* DATE: 2013/03/05
* DESCRIPTION: AvaluadorExpressions
*/

fun main() {
    val a=-2;
    val b=3;
    val c=1;
    val d=-3;

    println(2- a * b + d)
    println((2 - b) * a + c)
    println(b * a - d * a - c)
    println(d / 3 - b)
    println(b / (33 - a))
    println(a * 23 - 1 + c)
}