/*
* AUTHOR: Margaux
* DATE: 2013/03/05
* DESCRIPTION: IntercanviValors
*/

fun main() {
    var numA = 5;
    var numB = -5;

    val temp = numA
    numA = numB
    numB = temp

    println(numA)
    println(numB)
}